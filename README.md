# Some Addons for SpTBXLib

Some Addons for SpTBXLib, an external library for BorlandDeveloperStudio, such as Controls, Dialogs, Popups and Skins.

Author: Javier "j-a-s-d" Santo Domingo 

SourceForge project page: https://sourceforge.net/projects/sa4sptbx/

*** RUSSIAN ***

Сборка дополнений для SpTBXLib, сторонней библиотеки BorlandDeveloperStudio, такие как элементы управления, диалоговые и всплывающие окна, а так же стили. 

Автор: Javier "j-a-s-d" Santo Domingo 

Страница проекта на SourceForge: https://sourceforge.net/projects/sa4sptbx/